import requests
import json
import re
import csv
from bs4 import BeautifulSoup
from django.shortcuts import render
from django.contrib.auth.models import User
from django.http import HttpResponse
from tld import get_tld
# Create your views here.
def index(request):
    if request.method == "POST":
        url=request.POST.get("url", "")
        r = requests.get(url)
        soup = BeautifulSoup(r.content)
        name = soup.find_all("div",{"class":"FL w305 serpListLft"})
        phone = soup.find_all("div",{"class","phone"})
        name_list = []
        address_list = []
        phone_list= []
        for link in name:
            a = link.contents[1].find_all("a")[0].text.encode('ascii','ignore')
            name_list.append(a)
            b = link.find_all("p")[0].text
            b1 = b.replace('\n','').replace('\t','').replace('\r','')
            address_list.append(b1)

        for number in phone:
            c = number.text.replace('\n','').replace('\t','').replace('\r','')
            phone_list.append(c)

        list = zip(name_list,address_list,phone_list)
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="somefilename.csv"'
        writer = csv.writer(response)

        writer.writerow(['Name', 'Address', 'Contact No'])
        for name,add,phone in list:
             writer.writerow([name,add,phone])

        return response
    else:
        return render(request,'yellowpage.html',{})
    #return render(request,'index2.html',{'list': list,'address_list': address_list,})


def some_view(request):
    # Create the HttpResponse object with the appropriate CSV header.
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="somefilename.csv"'

    writer = csv.writer(response)
    writer.writerow(['First row', 'Foo', 'Bar', 'Baz'])
    writer.writerow(['Second row', 'A', 'B', 'C', '"Testing"', "Here's a quote"])

    return response
