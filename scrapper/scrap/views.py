import requests
import json
from bs4 import BeautifulSoup
from django.shortcuts import render
from django.contrib.auth.models import User
from django.http import HttpResponse
from tld import get_tld
# Create your views here.

def index(request):
    if request.is_ajax():
        response_data = {}
        url=request.POST.get("url", "")
        try:
            r = requests.get(url)
            soup = BeautifulSoup(r.content)
            source = get_tld(url)
            source_link = "http://" + source
            title = soup.find("title")
            images = soup.find_all("img")
            try:
                getmeta = soup.find("meta",{"name": "description"})
                description = getmeta.get("content")
            except:
                description =""
            image_list = []
            for img in images:
                image_list.append(img.get("src"))
            title = title.text
            response_data['title'] = title
            response_data['url'] = url
            response_data['image_list'] = image_list
            response_data['source'] = source
            response_data['source_link'] = source_link
            response_data['description'] = description
            return HttpResponse(json.dumps(response_data))
        except:
            error = "Enter a valid url"
            return render(request,'index.html',{'error': error})
    else:
        return render(request,'index.html',{})
